package main

import "fmt"

/*The main package is a special package which is used with the programs that are executable
and this package contains main() function. The main() function is a special type of function and it is
the entry point of the executable programs*/
func main() {
	fmt.Println("hello world")
	//declaring variable firstname and lastname they've string value
	var firstName string = "john"

	var lastName string
	lastName = "wick"
	//declare with 3 variable without declare type value.
	seventh, eight, ninth := "tujuh", "delapan", "sembilan"
	//Underscore (_) adalah reserved variable yang bisa dimanfaatkan untuk menampung nilai yang tidak dipakai.
	//Bisa dibilang variabel ini merupakan keranjang sampah.
	_ = "belajar Golang"
	_ = "Golang itu mudah"
	name, _ := "john", "wick"
	//Variabel name menampung data bertipe pointer string.
	nama := new(string)
	//Fungsi fmt.Printf() ini digunakan untuk menampilkan output dalam bentuk tertentu.
	//Kegunaannya sama seperti fungsi fmt.Println(), hanya saja struktur outputnya didefinisikan di awal.
	// karakter %s disitu akan diganti dengan data string yang berada di parameter ke-2, ke-3, dan seterusnya.
	fmt.Printf("halo %s %s!\n", firstName, lastName)

	fmt.Printf("halo john wick!\n")
	//Tanda plus (+) jika ditempatkan di antara string, fungsinya adalah untuk penggabungan string atau string concatenation.
	fmt.Println("halo", firstName, lastName+"!")

	fmt.Println("The Number", seventh, eight, ninth+"!")

	fmt.Println("Halo", name)
	//Jika ditampilkan yang muncul bukanlah nilainya melainkan alamat memori nilai tersebut (dalam bentuk notasi heksadesimal).
	fmt.Println(nama) // 0x20818a220
	// Untuk menampilkan nilai aslinya, variabel tersebut perlu di-dereference terlebih dahulu, menggunakan tanda asterisk (*).
	fmt.Println(*nama) // ""
}
